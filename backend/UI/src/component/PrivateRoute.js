// PrivateRoute.js
import React, { useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from './Utils/Context';

const PrivateRoute = ({ children }) => {
  const {auth, loading} = useAuth();
  if(loading) return <div>loading..</div>
  return auth ? children : <Navigate to="/login" />;
};

export default PrivateRoute;
