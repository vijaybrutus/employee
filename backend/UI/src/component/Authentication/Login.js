import { Fragment, useState } from "react";
import "./Login.css";
import { useAuth } from "../Utils/Context";
import { useNavigate } from "react-router-dom";
import AxiosInstance from "../Utils/AxiosInstance";
import axios from "axios";
const Login = () => {
    const {auth, login} = useAuth();
    const [authentication, setAuthentication] = useState({
        username:null,
        password:null
    });
    const navigate = useNavigate();
    const onHandleLogin=async(e)=>{
        e.preventDefault();
        await axios.post("http://localhost:8080/login", authentication
        ).then(response=>{
            login(response.data)
            navigate("/dashboard");
            //setAuthenticationValidation(true);
        }).catch(error=>console.log(error));
    }

    const onhandleChange=(event)=>{
        setAuthentication((existingVal)=>({...existingVal,[event.target.name]:event.target.value}));
    }

    return (
        <Fragment>
            <div className="p-4 w-50 m-auto mt-5 loginWrapper">
            <h2 className="d-flex justify-content-center p-3 pb-1 fw-bold">Login</h2>
           
                <div class="form-group p-3">
                    <label className="fw-bold mb-2" for="exampleInputEmail1" autoCorrect="false" >Email address</label>
                    <input onChange={onhandleChange} autocomplete="username" name="username" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                    <small id="emailHelp" class="form-text text-muted mt-2">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group p-3 pt-1">
                    <label className="fw-bold mb-2" for="exampleInputPassword1">Password</label>
                    <input onChange={onhandleChange} autocomplete="password" type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <button onClick={onHandleLogin}  type="submit" class="btn btn-primary w-50 d-flex justify-content-center m-auto text-center mt-3">Login</button>
         
            </div>
        </Fragment>
    )
}
export default Login;