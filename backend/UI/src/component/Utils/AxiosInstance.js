import axios from 'axios';

const AxiosInstance = axios.create({
  baseURL: 'http://localhost:8080', // Set your base URL here
  timeout: 5000, // Set your desired timeout
  headers: {
    'Content-Type': 'application/json',
    // You can add other common headers here
  },
});

// Add a request interceptor
AxiosInstance.interceptors.request.use(
  (config) => {
    // Get the token from local storage or wherever it's stored
    const token = localStorage.getItem('token');
    if (token) {
      // Set the Bearer token
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default AxiosInstance;