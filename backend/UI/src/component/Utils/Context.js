// AuthContext.js
import React, { createContext, useState, useContext, useEffect } from 'react';


const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(null);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      setAuth(token);
    }
    setLoading(false);
  }, []);

  const login = (token) => {
    const { employeeDetails, tokenDetails } = token
    localStorage.setItem('token', tokenDetails);
    console.log(employeeDetails, "token.employeeDetails")
    localStorage.setItem('userDetails', JSON.stringify(employeeDetails));
    setAuth(token);
  };

  const logout = () => {
    setAuth(null);
    localStorage.removeItem('token');
  };
  return (
    <AuthContext.Provider value={{ auth, loading, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
