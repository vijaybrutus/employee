import { Fragment, useState } from "react";
import AxiosInstance from "../Utils/AxiosInstance";
import { ToastContainer, toast } from 'react-toastify';
import { CustomScroll } from "react-custom-scroll";

const AddEmployee = ({setIsOpenEmployee, fetchEmployeeDetails, editemployeeDetails, setEditEmployee}) => {

    const [addEmployeeDetails, setEmployeeDetails] = useState({});

    const onSubmitAddEmployee = (e) => {
        e.preventDefault();
        AxiosInstance.post("/register", addEmployeeDetails)
            .then(response => {
                toast.success("User Created Successfully")
                fetchEmployeeDetails()
                setIsOpenEmployee(false)
            })
            .catch(err => console.log(err));
    }
    console.log(editemployeeDetails, "editemployeeDetails");
    const onHandleChange = (event) => {
        setEmployeeDetails((details) => ({ ...details, [event.target.name]: event.target.value }));
    }
    const onHandleeditEmployee=(e)=>{
        e.preventDefault();
        AxiosInstance.post("/editEmployee", addEmployeeDetails)
        .then(response => {
            toast.success("User Edited Successfully")
            fetchEmployeeDetails()
            setEditEmployee(null)
            setIsOpenEmployee(false)
        })
        .catch(err => console.log(err));
    }

    return (
        <Fragment>
            <div class="modalfixing p-4">
                <div class="modal-contentValue">
                    <div className="modal-header justify-content-between m-1 p-2" style={{borderBottom:"1px solid black"}}>
                            <h5 className="modal-title fw-bold" id="exampleModalLabel">Add Employee</h5>
                            <button type="button" className="btn-close" onClick={()=>{
                                setIsOpenEmployee(false)
                                setEditEmployee(null)
                                }}></button>
                        </div>
                        <div className="modalbody">
                            <div className="me-3 ms-0">
                                <div className="my-2">
                                    <label className="mb-2">Name</label>
                                    <input defaultValue={editemployeeDetails?.name} onChange={onHandleChange} type="text" autoComplete="name" name = "name" className="form-control" placeholder="Employee Name" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Gender</label>
                                    <select defaultValue={editemployeeDetails?.gender} onChange={onHandleChange}  name = "gender" autoComplete="gender"  className="form-select" aria-label="Default select example">
                                        <option selected>Select gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Birth date</label>
                                    <input defaultValue={editemployeeDetails?.dob} onChange={onHandleChange} name="dob" autoComplete="dob" type="date" className="form-control" placeholder="Employee birth date" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Department</label>
                                    <select defaultValue={editemployeeDetails?.dep} onChange={onHandleChange} name="dep" autoComplete="dep" className="form-select" aria-label="Default select example">
                                        <option selected>Select department</option>
                                        <option value="Operations">Operations</option>
                                        <option value="Management">Management</option>
                                        <option value="Security">security</option>
                                    </select>
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Role</label>
                                    <select defaultValue={editemployeeDetails?.role} onChange={onHandleChange} name="role" autoComplete="role" className="form-select" aria-label="Default select example">
                                        <option selected>Select Role</option>
                                        <option value="HR">HR</option>
                                        <option value="USER">USER</option>
                                        <option value="CEO">CEO</option>
                                    </select>
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Phone</label>
                                    <input defaultValue={editemployeeDetails?.phone} onChange={onHandleChange} name="phone" autoComplete="phone" type="phone" className="form-control" placeholder="Employee phone" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Email</label>
                                    <input defaultValue={editemployeeDetails?.email} onChange={onHandleChange} name="email" autoComplete="email"  type="text" className="form-control" placeholder="Employee Email" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div>
                              {!editemployeeDetails &&  <><div className="my-2">
                                    <label className="mb-2">Username</label>
                                    <input defaultValue={editemployeeDetails?.email} onChange={onHandleChange} name="username" autoComplete="username"  type="text" className="form-control" placeholder="Employee Username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div>
                                <div className="my-2">
                                    <label className="mb-2">Password</label>
                                    <input onChange={onHandleChange} name="password"  autoComplete="password" type="text" className="form-control" placeholder="Employee Password" aria-label="Recipient's username" aria-describedby="basic-addon2" />
                                </div></>}
                            </div>
                        </div>
                        <div className="d-flex justify-content-end gap-3 m-3">
                            <button type="button" className="btn btn-secondary" onClick={()=>setEmployeeDetails({})} >Cancel</button>
                            {!editemployeeDetails && <button type="button" className="btn btn-primary" onClick={onSubmitAddEmployee}>Add Employee</button>}
                            {editemployeeDetails && <button type="button" className="btn btn-primary" onClick={onHandleeditEmployee}>Edit Employee</button>}
                        </div>
                </div>
            </div>
        </Fragment>
    )
}
export default AddEmployee;