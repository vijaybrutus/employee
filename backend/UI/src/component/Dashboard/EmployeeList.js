import { Fragment } from "react";
import "./EmployeeList.css";
import AxiosInstance from "../Utils/AxiosInstance";
import { toast } from "react-toastify";

const EmployeeLists = ({employeeList, fetchEmployeeDetails, setIsOpenEmployee, editEmployee}) => {
    const onHandleEdit = () => { }
    const onHandleDelete = (username) => { 
        
        AxiosInstance.delete(`/deleteEmployee/${username}`).then(response=>{
            fetchEmployeeDetails();
            toast.success("Deleted SucessFully")
        }).catch(err=>{
            if(err.response.status === 403){
                toast.info("You don't have access to delete");
            } else{
                toast.error("Error in user deleted");
            }
        })
    }
    return (
        <Fragment>
            <table className="p-5 m-5">
                <thead>
                    <tr>
                        <th scope="col">S.No</th>
                        <th scope="col">Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">DOB</th>
                        <th scope="col">Department</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody className="m-3">
                    {employeeList.map((val, key) => {
                        return (
                            <tr className="m-3 p-5">
                                <th scope="row">{key + 1}</th>
                                <td>{val.name}</td>
                                <td>{val.gender}</td>
                                <td>{val.dob}</td>
                                <td scope="col">{val.dep}</td>
                                <td scope="col">{val.phone}</td>
                                <td scope="col">{val.email}</td>
                                <td className="d-flex w-auto m-auto justify-content-center gap-2">
                                    <button className="px-4 btn btn-primary" onClick={()=>{
                                        setIsOpenEmployee(true)
                                        editEmployee(key)
                                    }}>Edit</button>
                                    <button className="px-4 btn btn-outline-secondary" onClick={()=>onHandleDelete(val.username)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        
        </Fragment>
    )
}
export default EmployeeLists;