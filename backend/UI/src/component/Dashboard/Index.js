import React, { Fragment, useEffect, useState } from "react";
import EmployeeLists from "./EmployeeList";
import { useAuth } from "../Utils/Context";
import AddEmployee from "./AddEmployee";
import AxiosInstance from "../Utils/AxiosInstance";
const Dashboard = () => {
    const { logout } = useAuth();
    const [userDetails, setUserDetails] = useState({});

    const [isOpenAddEmployee, setIsOpenEmployee] = useState(false);
    const [employeeList, setEmployeeList] = useState([]);
    const [editemployeeDetails, setEditEmployee] = useState(null);

    const fetchEmployeeDetails = () => {
        AxiosInstance.get("/employees").then(response => setEmployeeList(response.data)).catch(err => console.log(err));
    }

    useEffect(() => {
        fetchEmployeeDetails()
        const storedObjectString = localStorage.getItem("userDetails");
        const myObject = JSON.parse(storedObjectString);
        setUserDetails(myObject);
    }, []);

    const editEmployee=(keyval)=>setEditEmployee(employeeList[keyval])

    console.log(editemployeeDetails, "editemployeeDetails");
    return (
        <Fragment>
            <div className="d-flex align-items-center justify-content-between p-4">
                <h2 className='fw-bold mr-auto'>Employee Details</h2>
                <div className="d-flex gap-5 align-items-center">
                    {userDetails?.authorities && <div>
                        <span>UserName : <strong>{userDetails?.name}</strong></span><br />
                        <span>Role : <strong>{userDetails?.authorities[0]?.authority}</strong></span>
                    </div>}
                    <button onClick={logout} className="btn btn-primary ms-auto">Logout</button>
                </div>

            </div>
            <div className='d-flex align-items-center justify-content-end me-3 gap-4'>
                <div className='d-flex'>
                    <button className="btn btn-success" onClick={() => setIsOpenEmployee(true)}>Add Employee</button>
                </div>
            </div>
            {isOpenAddEmployee && <AddEmployee setEditEmployee={setEditEmployee} editemployeeDetails={editemployeeDetails}  fetchEmployeeDetails={fetchEmployeeDetails} setIsOpenEmployee={setIsOpenEmployee} />}
            <EmployeeLists editEmployee={editEmployee} fetchEmployeeDetails={fetchEmployeeDetails} employeeList={employeeList} setIsOpenEmployee={setIsOpenEmployee}/>
        </Fragment>
    )
};
export default Dashboard;