import logo from './logo.svg';
import './App.css';
import { Fragment, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import Login from './component/Authentication/Login';
import Dashboard from './component/Dashboard/Index';
import { AuthProvider } from './component/Utils/Context';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import PrivateRoute from './component/PrivateRoute';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
function App() {
  return (
    <Fragment>
      <AuthProvider>
        <Router>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/" element={<Navigate to="/dashboard" replace />} /> {/* Redirect from root to dashboard */}
            <Route 
            path={"/dashboard"}
            element={
              <PrivateRoute>
                <Dashboard />
              </PrivateRoute>
            }
          />
            {/* Add other private routes here */}
          </Routes>
        </Router>
        <ToastContainer />
      </AuthProvider>

    </Fragment>
  );
}

export default App;
