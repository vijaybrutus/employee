package com.employee.details.Service;

import com.employee.details.DTOPattern.AuthenticationDTO;
import com.employee.details.modal.AuthenticationResponse;
import com.employee.details.modal.EmployeeDetails;
import com.employee.details.modal.PasswordChange;
import com.employee.details.modal.Token;
import com.employee.details.repository.EmployeeRepository;
import com.employee.details.repository.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final TokenRepository tokenRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    // register
    public String register(EmployeeDetails request) throws Exception{
        EmployeeDetails employeeDetails = EmployeeDetails.builder()
                .email(request.getEmail())
                .DOB(request.getDOB())
                .dep(request.getDep())
                .name(request.getName())
                .role(request.getRole())
                .password(passwordEncoder.encode(request.getPassword()))
                .phone(request.getPhone())
                .gender(request.getGender())
                .username(request.getUsername())
                .build();
       employeeRepository.save(employeeDetails);
       return "User Created Successfully";
    }
    // authentication
    public AuthenticationResponse authenticate(AuthenticationDTO request) throws Exception{
        System.out.println(" I AM Crossed "+ request.getUsername() + request.getPassword() );
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()));
        EmployeeDetails employeeDetails = employeeRepository.findByUsername(request.getUsername()).orElseThrow();
        System.out.println(" I AM Crossed next" + employeeDetails);
        String jwt = jwtService.generateJwtToken(employeeDetails);
        System.out.println(" I AM Crossed next" + jwt);
        return new AuthenticationResponse(jwt, employeeDetails);
    }
    // get allList
    public List<EmployeeDetails> getAllEmployeeDetails(String token){
        System.out.println(token + "  Token validToken");
            return employeeRepository.findAll();
    }
    // delete employee
    @Transactional
    public String deleteEmployeeDetails(String username){
        try{
            employeeRepository.removeByUsername(username);
            return "Employee Deleted Successfully";
        } catch (Exception e){
            return "No user found";
        }


    }
    // edit details
    public String editEmployee( EmployeeDetails request){

        try{
            EmployeeDetails employeeDetails = employeeRepository.findByUsername(request.getUsername()).orElseThrow();
            EmployeeDetails.builder()
                        .email(request.getEmail())
                        .DOB(request.getDOB())
                        .dep(request.getDep())
                        .name(request.getName())
                        .role(request.getRole())
                        .phone(request.getPhone())
                        .gender(request.getGender())
                        .username(employeeDetails.getUsername())
                        .build();
                employeeRepository.save(employeeDetails);
                return "Employee Updated Successfully";

        } catch (Exception e){
            return "Not Exists";
        }
    };
    // edit password
    public String editPassword(String token, PasswordChange request){
        try{
                EmployeeDetails existingEmployeeDetails = employeeRepository.findByUsername(request.getUsername()).orElse(null);
                if(existingEmployeeDetails.getUsername() !=null && existingEmployeeDetails.getPassword().equals(request.getOldPassword())){
                    existingEmployeeDetails.setPassword(request.getNewPassword());
                    return "Password Updated Successfully";
                }
            return "Password Incorrect";
        } catch (Exception e){
            return "Not Exists";
        }
    }

}
