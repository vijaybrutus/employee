package com.employee.details.controller;
import com.employee.details.DTOPattern.AuthenticationDTO;
import com.employee.details.Service.EmployeeService;
import com.employee.details.Service.JwtService;
import com.employee.details.filter.JwtAuthenticationFilter;
import com.employee.details.modal.AuthenticationResponse;
import com.employee.details.modal.EmployeeDetails;
import com.employee.details.modal.PasswordChange;
import com.employee.details.util.BearerToken;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
public class EmployeeController {
    private final JwtService jwtService;
    private final EmployeeService employeeService;
    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final BearerToken bearerToken;
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> Authentication(@RequestBody AuthenticationDTO request) throws Exception {
        return ResponseEntity.ok(employeeService.authenticate(request));
    }
    // CREATE EMPLOYEE
    @PostMapping("/register")
    public ResponseEntity<String> createEmployee(@RequestBody EmployeeDetails request) throws Exception {
        return ResponseEntity.ok(employeeService.register(request));
    }
    // DELETE EMPLOYEE
    @DeleteMapping("/deleteEmployee/{username}")
    public ResponseEntity<String> deleteEmployee( @PathVariable String username) throws Exception {
        return ResponseEntity.ok(employeeService.deleteEmployeeDetails(username));
    }
    // EDIT EMPLOYEE
    @PostMapping("/editEmployee")
    public ResponseEntity<String> editEmployee(@RequestBody EmployeeDetails request){
        return  ResponseEntity.ok(
                employeeService.editEmployee( request));
    }
    // VIEW EMPLOYEE
    @GetMapping("/employees")
    public ResponseEntity<List<EmployeeDetails>> viewEmployee(HttpServletRequest httpReq){
        System.out.println("I M In Checking");
        return ResponseEntity.ok(employeeService.getAllEmployeeDetails(bearerToken.tokenAlter(httpReq)));
    }
    // CHANGE PASSWORD
    @PostMapping("/changePassword")
    public ResponseEntity<String> changePassword(HttpServletRequest httpReq, @RequestBody PasswordChange passwordUpdate){
        return ResponseEntity.ok(employeeService.editPassword(bearerToken.tokenAlter(httpReq), passwordUpdate));
    }
}
