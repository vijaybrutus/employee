package com.employee.details.repository;

import com.employee.details.modal.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<EmployeeDetails, String> {
    Optional<EmployeeDetails> findByUsername(String username);
    void removeByUsername(String username);
}
