package com.employee.details.modal;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.UUID;
@Entity
@Getter
@Setter
public class Token {
    @Id
    @GeneratedValue(generator = "UUID")
    private UUID id;
    private String token;
    private boolean isLoggedOut;
    @ManyToOne
    @JoinColumn(name = "employeeId")
    private EmployeeDetails employeeDetails;
}
