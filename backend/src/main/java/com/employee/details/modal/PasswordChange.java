package com.employee.details.modal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class PasswordChange {
    private String username;
    private String oldPassword;
    private String newPassword;
}
