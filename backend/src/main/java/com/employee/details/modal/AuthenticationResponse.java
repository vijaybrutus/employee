package com.employee.details.modal;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AuthenticationResponse {
    private final String tokenDetails;
    private final EmployeeDetails employeeDetails;
}
