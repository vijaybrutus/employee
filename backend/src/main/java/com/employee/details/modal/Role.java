package com.employee.details.modal;

public enum Role {
    HR,
    CEO,
    USER
}
